import { NowRequest, NowResponse } from '@now/node'
import fetch from 'node-fetch'
import _ from 'lodash'

function get(offset) {
  console.log(`geting with offset ${offset}`)
  return fetch(`https://api.airtable.com/v0/appEPrcgMuBtgaKfU/Origin-destination?maxRecords=1000&api_key=${process.env.AIRTABLE_KEY}&offset=${offset}`)
  .then(raw => raw.json())
  .then(r =>  {
    const current = _(r.records).map(od => [od.fields.Origin + od.fields.Destination, od.fields]).fromPairs().value()
    if (r.offset) {
      return get(r.offset).then(moar => Object.assign(current, moar))
    } else {
      return current
    }
  })
}

export default (req: NowRequest, res: NowResponse) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  const result = {}
  get('')
  .then(data => res.json(data))
}

