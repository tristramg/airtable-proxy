import { NowRequest, NowResponse } from '@now/node'
import fetch from 'node-fetch'
import _ from 'lodash'

export default (req: NowRequest, res: NowResponse) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  fetch(`https://api.airtable.com/v0/appEPrcgMuBtgaKfU/Cities?api_key=${process.env.AIRTABLE_KEY}`)
  .then(raw => raw.json())
  .then(rawCities =>  _(rawCities.records).map(c => [c.id, c.fields]).fromPairs().value())
  .then(data => res.json(data))
}

